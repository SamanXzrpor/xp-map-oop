
	var map = L.map('map');

	L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		maxZoom: 18,
		attribution: 'Map data &copy; <a href="https://www.techroot.ir/">TCR</a> contributors, ',
		id: 'mapbox/streets-v11',
		tileSize: 512,
		zoomOffset: -1
	}).addTo(map);

	// set View 
	map.setView([37.524, 45.073], 12);

	// Add marker and Popup tp map
    // L.marker([37.520,45.070]).addTo(map).bindPopup('منزل حاجی');

	// Use GeoLocation API For Give user Position

	var current_position,current_accuracy;
	map.on('locationfound',function(e) {
		if(current_position){
			map.removeLayer(current_position);
			map.removeLayer(current_accuracy);
		}
		var radius = e.accuracy / 2;

		current_position = L.marker(e.latlng).addTo(map).bindPopup('دقت تقریبی :'+ radius +'متر');
        current_accuracy = L.circle(e.latlng,radius).addTo(map);
	})
	function locate(){
		map.locate({setView: true ,maxZoom: 14});
	}
	

	$(".user-location").on("click",function(){
		setTimeout(locate,1000);
	})

	// Give lat lon and Get BoundLine

	var northLine = map.getBounds().getNorth();
    var southLine = map.getBounds().getSouth();
    var westLine  = map.getBounds().getWest();
    var eastLine  = map.getBounds().getEast();

