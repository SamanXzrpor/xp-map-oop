<?php
namespace App\Classes;
include __DIR__ . '/../../autoload.php';
session_start();

use App\Traits\BaseUrl;
use App\Traits\Redirect;


class Login extends DBOprations{

    use BaseUrl;
    use Redirect;

    public function getInfoUsers($email)
    {
        $this->select("SELECT * FROM users WHERE email = '".$email."'");
        
    }

    public function login($data)
    {
        if($data['email'] == $this->results->email){
            if (password_verify($data['password'],$this->results->password)) {
                if($this->results->permision == 1){
                    $_SESSION['login'] = $this->results;
                    $this->redirect('panel.php');
                }else{
                    $_SESSION['login'] = $this->results;
                    $this->redirect('index.php');
                }
                
                
            }else{
                $this->redirect('login.php');

            }
        }else{
            $this->redirect('login.php');
        }
    }

    public static function checkLogin()
    {
        return isset($_SESSION['login']) ? true : false;
    }

    public static function currentUser()
    {
        return $_SESSION['login']->id;
    }

    public static function logOut()
    {
        session_unset();
        session_destroy();
    }
}
