<?php
namespace App\Classes;
include __DIR__ . '/../../autoload.php';
use PDO;
use PDOException;

class DBOprations extends Config implements Oprations{

    public $results;
    public $infos;

    public function insert($data)
    {
        $stmt = $this->db->prepare($data);
        $stmt->execute();
    }
    
    public function select($data)
    {
        $stmt = $this->db->prepare($data);
         $stmt->execute();
        
        $info = $stmt->fetchAll(PDO::FETCH_OBJ);
        foreach ($info as $results => $result) {
            $this->results = $result;
            $this->infos = $info;
        }
    }

    public function update($data)
    {
        $stmt = $this->db->prepare($data);
        $stmt->execute();
        
    }
    public function delete($data)
    {
        $stmt = $this->db->prepare($data);
        $stmt->execute();
        $this->results = $stmt->fetchAll(PDO::FETCH_OBJ);
    }

}
