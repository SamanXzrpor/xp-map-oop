<?php
namespace App\Classes;
include __DIR__ . '/../../autoload.php';
use APP\Classes\Login;

class Location extends DBOprations{


    public function addLocation(array $data)
    {
        $this->insert("INSERT INTO locations (user_id,title,lat,lng,types,verified) VALUES
         ('".Login::currentUser()."','{$data['title']}','{$data['lat']}','{$data['lng']}','{$data['types']}',0)");

         
    }
    
    public function getLocations($params = [])
    {
        if(isset($params['verified']) AND in_array($params['verified'],[0,1])){

            $this->select("SELECT * FROM locations WHERE verified = '{$params['verified']}'");

        }else{

            $this->select("SELECT * FROM locations");

        }
        return $this->infos;
        
    }

    public function previewLocation($id)
    {
        $this->select("SELECT title,lat,lng FROM locations WHERE id = $id");
        return $this->results;
    }

    public function verifyLocation($id)
    {
        $this->update("UPDATE locations SET verified= 1 - verified WHERE id = $id");
    }

}

