<?php
namespace App\Classes;
include __DIR__ . '/../../autoload.php';


class Search extends DBOprations
{


    public function searchLocation(string $data)
    {
        if(strlen(trim($data)) > 3 ){
            $sql = "SELECT * FROM locations WHERE title LIKE '%{$data}%' AND verified = 1";
            $this->select($sql);
            return $this->infos;
        }
        
    }

}
