<?php
namespace App\Classes;

use PDO;
use PDOException;

abstract class Config {

    private $dbname = "xp-map-oop";
    private $host   = "localhost";
    private $user   = "root";
    private $pass = "";
    protected $db;

    public function __construct()
    {
        try {
            $this->db = new PDO("mysql:host={$this->host};dbname={$this->dbname};charset=utf8",
            $this->user,$this->pass);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            return "Error Exception: " . $e->getMessage();
        }

    }
}

