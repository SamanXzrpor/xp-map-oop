<?php

include __DIR__ . '/../autoload.php';

use App\Classes\Register;
use App\Classes\Login;
use App\Classes\Location;
use App\Classes\Helpers;
use App\Classes\Search;
use App\bootstrap\LocationTypes;


# Register User By Informations
$registerHandler = new Register();

if(isset($_POST['register-sub'])){

    $registerHandler->registerUser($_POST);
    $registerHandler->redirect('login.php');
    
}

# Check that User is Login Or Not 
$LoginHandel = new Login();

if(isset($_POST['submitLogin'])){
    $LoginHandel->getInfoUsers($_POST['email']);

    $LoginHandel->login($_POST);

}

# Insert New Location By users
$locationHandel = new Location();
if(isset($_POST['submitLoc'])){

    $locationHandel->addLocation($_POST);

    Helpers::redirect('index.php');

} 
    # verify location by admin 
if(isset($_POST['loc'])){
    $locationHandel->verifyLocation($_POST['loc']);
}

 # Search Process 
 $searchHandel = new Search();
 $locationTypes = new LocationTypes();
 if(isset($_POST['search']) AND !empty($_POST['search'])){
     $results = $searchHandel->searchLocation($_POST['search']);
    if($results){
        if(strlen(trim($_POST['search'])) > 3){
            foreach($results as $key => $value){
                echo "<a href='".Helpers::baseUrl()."?preview={$value->id}'>
                    <div class='result-item' data-id='{$value->id}' data-lat='{$value->lat}' data-lng='{$value->lng}'>
                        <span class='loc-type'>{$locationTypes->getTypes()[$value->types]}</span>
                        <span class='loc-title'>{$value->title}</span>
                    </div>
                </a>";    
            }
         }
    }else{
        echo "در حال جست و جوی مکان ...";
    }
     
 }

