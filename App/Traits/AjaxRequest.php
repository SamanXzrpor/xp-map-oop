<?php
namespace App\Traits;

trait AjaxRequest {

    public static function ajaxVerify(){

        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
        {    
            return true;
        }
        
    }
}