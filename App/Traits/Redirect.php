<?php
namespace App\Traits;
include __DIR__ . '/../../autoload.php';

use App\Traits\BaseUrl;
trait Redirect {

    use BaseUrl;

    public static function redirect(string $dir) 
{
    header("Location: ".self::baseUrl()."$dir");
}

}