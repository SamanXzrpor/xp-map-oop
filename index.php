<?php
include 'autoload.php';
use App\Classes\Helpers;
use App\Classes\Location;
use App\Classes\Search;
use App\bootstrap\LocationTypes;


# Get Location Type
$locationTypes = LocationTypes::getTypes();

# preview data 
$previewData = null;
$locationHandel = new Location();

if(isset($_GET['preview'])){

    $previewData = $locationHandel->previewLocation($_GET['preview']);
    
    
}

include 'tpl/index.php';