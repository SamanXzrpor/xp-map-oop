<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>XP-Map</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"/>
    <script defer src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" ></script>
    <script defer src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="assets/css/style.css"/>
    <link rel="stylesheet" href="assets/css/styles.css"/>
    <link rel="stylesheet" href="assets/css/leaflet.css"/>

</head>
<body>

        <!-- Example single danger button -->
    <div class="btn-group">
    <button type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">
       ورود 
    </button>
    <ul class="dropdown-menu">
        <li><a class="dropdown-item" href="register.php">Register</a></li>
        <li><a class="dropdown-item" href="login.php">Login</a></li>
        <li><hr class="dropdown-divider"></li>
    </ul>
    </div>

    <!-- Open Modal Foe Add new location -->
     <div class="main-modal"> 
        <p class="close-modal">X</p>
        <h2>اضافه کردن موقعیت مکانی </h2>
        <form action="App/process.php" method="post">
            <input type="text" name="lat" class="lat" readonly placeholder="lat">
            <input type="text" name="lng" class="lng" readonly placeholder="lng"><br><br>
            <input type="text" name="title" id="title" placeholder="اطلاعات مکان"><br><br>
            <select name="types">
                <?php
                foreach ($locationTypes as $key => $value) :?>
                <option value="<?= $key ?>"><?= $value?></option>
                <?php endforeach; ?>
            </select><br><br>
            
            <div id="submit">
            <button type="submit" name="submitLoc">اضافه کردن</button>
            </div>
        </form>
    </div> 

    <!--  Implemant site for Show Map -->
    <div class="main">
        <div class="head">
         <input type="text" id="search" placeholder="دنبال کجا میگردی؟" autocomplete="off">
            <div id="result-search">
                
            </div>
        </div>

      <div class="mapContainer">
          <div id="map" style="width: 100%; height: 700px"></div>
          <img src="assets/img/current.png" class="user-location">
      </div>
    </div>
<script  src="assets/js/leaflet.js" ></script>
<script  src="assets/js/main.js" ></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" ></script>
<script  src="assets/js/scripts.js"></script>

<script>

  
<?php if(!is_null($previewData)): ?>
    L.marker([<?= $previewData->lat; ?>,<?= $previewData->lng; ?>]).addTo(map).bindPopup("ّ<?= $previewData->title; ?>").openPopup();
<?php endif; ?>

$(document).ready(function(){

        	// Open Insert location Modal 
	map.on('dblclick',function(event){

    // add marker in position
        L.marker(event.latlng).addTo(map);

    //open modal form
        $(".main-modal").fadeIn(500);
    // add latlng to form by JS
        $(".lat").val(event.latlng.lat);
        $(".lng").val(event.latlng.lng);
    });

        // Fade out the modal
	    $(".close-modal").click(function(){
		$(".main-modal").fadeOut(500);
        location.reload();
	    })

        $("#search").keyup(function(e){

            var searchText = $("#search").val();
            $("#result-search").fadeIn(500);
            $(".loc-title").html("در حال جست و جوس مکان ... ");
            
            $.ajax({
                url : "App/Process.php",
                method : "POST",
                data : {search:searchText},
                success : function(response){
                    $("#result-search").slideDown().html(response);
                },
            })
        });

        $(".mapContainer").click(function(){
		$("#result-search").fadeOut(200);
        
	    });

     
});



</script>
</body>
</html>