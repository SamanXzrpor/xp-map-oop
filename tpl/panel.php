<?php 
    include __DIR__ . '/../vendor/autoload.php';
    use Hekmatinasser\Verta\Verta;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>7Map Panel</title>
    <link href="favicon.png" rel="shortcut icon" type="image/png">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"/>
    <script defer  src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" ></script>
    <script defer  src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="assets/css/styles.css<?="?v=" . rand(99, 9999999)?>" />
    <style>
    body{
        background:#f2f2f2;
    }
    a{
        text-decoration: none;
    }
    h1{
        text-align: center;
    }

    .main-panel{
        width:1000px;
        margin:30px auto;
    }
    .box {
        background: #fff;
        padding: 10px 20px;
        border-radius: 5px;
        box-shadow: 0px 3px 3px #EEE;
        margin-bottom: 20px;
        direction: rtl;
    }
    table.tabe-locations {
        width: 100%;
        border-collapse: collapse;
    }
    .statusToggle {
        background: #eee;
        color: #686868;
        border: 0;
        padding: 3px 12px;
        border-radius: 20px;
        cursor: pointer;
        font-size: 13px;
        font-weight: 400;
        font-family: iransans;
        display:inline-block;
        margin:0 3px;
        min-width: 70px;
        text-align: center;
    }
    .statusToggle.active {
        background: #0c8f10;
        color: #ffffff;
    }
    .statusToggle:hover,button.preview:hover {
        opacity: 0.7;
    }
    button.preview {
        padding: 0 10px;
        background: none;
        border: none;
        font-size: 20px;
        cursor: pointer;
    }
    tr {
        line-height: 36px;
    }
    tr:nth-child(2n) {
        background:#f7f7f7;
    }
    td{
        padding:0 5px;
    }
    iframe#mapWivdow {
        width: 100%;
        height: 500px;
    }
    .text-center{
        text-align: center;
    }
    #preview-modal{
        display: none;
        position: absolute;
        padding: 15px 20px;
        height: 300px;
        width: 740px;
        left: 318px;
        top: 100px;
        border-radius: 10px;
        border-right: 3px solid;
        border-left: 3px solid;
    }
    .modal-content{
        width: 99%;
        height: 99%;
        position: absolute;
        top: 0px;
        left: 3px;
        border-radius: 10px;
    }
    iframe#mapWivdow {
    width: 100%;
    height: 327px;
    border-radius: 8px;
    }
    .close-preview{
        z-index: 998;
        position: absolute;
        left: 738px;
        top: -16px;
        font-family: cursive;
        font-weight: bold;
        font-size: larger
    }
    .close-preview:hover{
        cursor: pointer;
    }

    </style>
</head>
<body>
    <div class="main-panel">
        <h1>پنل مدیریت <span style="color:#007bec">ایکس پی مپ</span></h1>
        <div class="box">
            <a class="statusToggle" href="http://localhost/XP-Map-OOP/panel.php" >🏠</a>
            <a class="statusToggle active" href="?verified=1">فعال</a>
            <a class="statusToggle" href="?verified=0">غیرفعال</a>
            <a class="statusToggle" href="http://localhost/XP-Map-OOP" style="float:left">خروج</a>
        </div>
        <div class="box">
        <table class="tabe-locations">
        <thead>
        <tr>
        <th style="width:40%">عنوان مکان</th>
        <th style="width:15%" class="text-center">تاریخ ثبت</th>
        <th style="width:10%" class="text-center">lat</th>
        <th style="width:10%" class="text-center">lng</th>
        <th style="width:25%">وضعیت</th>
        </tr>
        </thead>
        <tbody>
            <?php if($locations !== null): ?>
        <?php foreach ($locations as $loc):?>
        <tr>
            <td><?= $loc->title ?></td>
            <td class="text-center"><?= Verta::instance( $loc->created_at)->format('%d - %B - %Y')?></td>
            <td class="text-center"><?= $loc->lat ?></td>
            <td class="text-center"><?= $loc->lng ?></td>
            <td class="verify-tags"> 
                
                <button class="statusToggle activation <?= $loc->verified ? 'active' : '' ?>" data-loc="<?= $loc->id?>" ><?= $loc->verified ? 'فعال' : 'غیرفعال' ?></button>
                <button class="preview" data-loc='<?= $loc->id ?>'>👁️‍🗨️</button> 
                
            </td>
        </tr>
        <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
        </table>
        </div>

        <div id="preview-modal">
            <h4 class="close-preview">X</h4>
            <div class="modal-content">
                <iframe id='mapWivdow' src="#" frameborder="0"></iframe>
            </div>
        </div>

    </div> 



    <script  src="assets/js/jquery.min.js"></script>
    <script>
    

        $(document).ready(function(){

            $(".preview").click(function(){
                var dataLoc = $(this).attr('data-loc');

                $("#preview-modal").fadeIn(1000);
                $("#mapWivdow").attr('src','http://localhost/XP-Map-OOP/?preview=' + dataLoc);
            })
        

            $(".close-preview").click(function(e){
                
                $("#preview-modal").fadeOut(500);
            })


            $(".activation").click(function(event){

                event.preventDefault();
            
                const locId = $(this).attr("data-loc");

                $.ajax({
                    url: "App/Process.php",
                    method: "POST",
                    data: {loc:locId},
                    success: function(response){
                        location.reload();
                    }
                })
            })
        })
    </script>
</body>
</html>
